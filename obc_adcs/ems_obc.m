addpath('~/opt/openEMS/share/openEMS/matlab');
addpath('~/opt/openEMS/share/CSXCAD/matlab');
addpath('~/opt/openEMS/share/hyp2mat/matlab');
addpath('~/opt/openEMS/share/CTB/matlab');

close all
clear
clc

physical_constants;

%% Setup the simulation
physical_constants;
unit = 1e-3; % all length in mm

fstart = 1e4; % [Hz]
f0  = 2e4; % [Hz]
fstop  = 5e4; % [Hz]

lambdastart = c0/fstart/unit;
lambda0 = c0/f0/unit;
lambdastop = c0/fstop/unit;

cell.maxsize = lambdastop/10;
cell.bound_margin = lambdastop/(2*10);

pcb.width = 95.89/unit;
pcb.height = 90/unit
pcb.thickness = 1.6/unit;
sheidl.width = pcb.width;
shield.height = pcb.height;
shield.thickness = 1/unit;
shield.z = 30/unit;
emission.z = shield.z + 10/unit;

bound.start.x = -pcb.width - cell.bound_margin; 
bound.start.y = -pcb.height - cell.bound_margin;
bound.start.z = - cell.bound_margin;

bound.stop.x = pcb.width + cell.bound_margin;
bound.stop.y = pcb.height + cell.bound_margin;
bound.stop.z = emission.z + cell.bound_margin; 

dipole.length = 0.48 * lambda0/4; 
feed.width  = 1/unit; 
feed.z = shield.z + 10/unit

FDTD = InitFDTD('NrTS',1000000, 'EndCriteria',1e-4);

% Initialize geometry
CSX = InitCSX();

% Initial mesh
mesh.x = bound.start.x:cell.maxsize:bound.stop.x;
mesh.y = bound.start.y:cell.maxsize:bound.stop.y;
mesh.z = bound.start.z:cell.maxsize:bound.stop.z;
CSX = DefineRectGrid(CSX, unit, mesh);

%% Dump Box
CSX = AddDump(CSX,'Et','FileType',1,'SubSampling','2,2,2');
start = [mesh.x(1)   mesh.y(1)   mesh.z(1)];
stop  = [mesh.x(end) mesh.y(end) mesh.z(end)];
CSX = AddBox(CSX,'Et', 0, start, stop);


%% Excitation
FDTD = SetGaussExcite(FDTD, 0.5*(fstart + fstop),0.5*(fstop - fstart));
%CSX = AddExcitation( CSX, 'mtq', 2, [0 0 3] );

ports.start=[-feed.width/2   -feed.width/2   feed.z];
ports.stop =[feed.width/2 feed.width/2 feed.z + 10/unit]
[CSX, port{1}] = AddRectWaveGuidePort(CSX, 0, 1, ports.start, ports.stop, 'z',
                lambdastop/20, lambdastop/20, 'TE10', 1);

%% Create PCB

% FR-4 definition
CSX = AddMaterial(CSX,'FR4');
CSX = SetMaterialProperty( CSX, 'FR4', 'Epsilon', 4.4, 'Mue', 1 );

CSX = AddBox(CSX, 'FR4', 0, 
  [-pcb.width/2, -pcb.height/2, 0], 
  [pcb.width/2, pcb.height/2, pcb.thickness]
  );

% thin PEC layer
CSX = AddMetal( CSX, 'PEC' ); % create a perfect electric conductor (PEC)
CSX = AddBox(CSX, 'PEC', 0, 
  [-pcb.width/2 + pcb.width /100, -pcb.height/2 + pcb.height/100, pcb.thickness], 
  [pcb.width/2 - pcb.width /100, pcb.height/2 - pcb.height/100, 1.1*pcb.thickness]
  );
  
% Define shield boundaries
% OpenEMS doesn't have boolean operations so we have to define the walls one 
% by one

% TODO: Determine used material and set ε and μ to accurate values
CSX = AddMaterial(CSX,'Aluminum');
CSX = SetMaterialProperty( CSX, 'Aluminum', 'Epsilon', 5, 'Mue', 5);

CSX = AddBox(CSX, 'Aluminum', 0, 
  [-pcb.width/2, -pcb.height/2, pcb.thickness], 
  [pcb.width/2 + shield.thickness, -pcb.height/2 - shield.thickness, pcb.thickness + shield.z]
  );
CSX = AddBox(CSX, 'Aluminum', 0, 
  [pcb.width/2, -pcb.height/2, pcb.thickness], 
  [pcb.width/2 + shield.thickness,  pcb.height/2 + shield.thickness, pcb.thickness + shield.z]
  );
CSX = AddBox(CSX, 'Aluminum', 0, 
  [pcb.width/2, pcb.height/2, pcb.thickness], 
  [-pcb.width/2 - shield.thickness,  pcb.height/2 + shield.thickness, pcb.thickness + shield.z]
  );
CSX = AddBox(CSX, 'Aluminum', 0, 
  [-pcb.width/2, pcb.height/2, pcb.thickness], 
  [-pcb.width/2 - shield.thickness,  -pcb.height/2 - shield.thickness, pcb.thickness + shield.z]
  );
CSX = AddBox(CSX, 'Aluminum', 0, 
  [-pcb.width/2, -pcb.height/2, pcb.thickness], 
  [pcb.width/2,  pcb.height/2, pcb.thickness + shield.z]
  );

%CSX = AddBox(CSX, 'mtq', 1, [-lambdastop/100, -lambdastop/100, emission.z], 
%                             [lambdastop/100, lambdastop/100, emission.z]);

% Boundary condition
BC = [1 1 1 1 1 1]  
FDTD = SetBoundaryCond(FDTD, BC);

mesh = DetectEdges(CSX, mesh);
mesh = SmoothMesh(mesh, c0 / f0 / unit / 20 );
CSX = DefineRectGrid(CSX, unit, mesh);

%% Write openEMS compatoble xml-file %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Sim_Path = 'tmp_mod';
Sim_CSX = 'rect_wg.xml';

[status, message, messageid] = rmdir(Sim_Path,'s');
[status, message, messageid] = mkdir(Sim_Path);

WriteOpenEMS([Sim_Path '/' Sim_CSX], FDTD, CSX);

CSXGeomPlot([Sim_Path '/' Sim_CSX]);
RunOpenEMS(Sim_Path, Sim_CSX);

%% Post-processing

%freq = linspace(fstart,fstop,201);
%port = calcPort(port, Sim_Path, freq);

%s11 = port{1}.uf.ref./ port{1}.uf.inc;


%figure
%dump_file = [Sim_Path '/Et.h5'];
%PlotArgs.slice = {0 0 0};
%PlotArgs.pauseTime=0.01;
%PlotArgs.component=0;
%PlotArgs.Limit = 'auto';
% PlotHDF5FieldData(dump_file, PlotArgs)