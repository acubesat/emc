$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2021
		MinorVer=1
		Name='Solution Process'
		StartInfo='Time:  10/09/2022 02:09:28; Host: cn12.it.auth.gr; Processor: 20; OS: Linux 3.10.0-1160.66.1.el7.x86_64; Maxwell 3D 2021.1.0'
		TotalInfo='Elapsed time : 00:11:44 , ComEngine Memory : 349 M'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /mnt/apps/prebuilt/AnsysEM/2021R1/AnsysEM21.1/Linux64/MAXWELLCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Desired RAM limit not set.\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:01:19 , Maxwell 3D ComEngine Memory : 338 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('  Mesh TAU', 126, 0, 126, 0, 1814000, 'I(1, 0, \'1494078 tetrahedra\')', true, true)
		ProfileItem('  Mesh Post(TAU)', 115, 0, 115, 0, 1814000, 'I(1, 0, \'322686 tetrahedra\')', true, true)
		ProfileItem('  Mesh Post', 29, 0, 28, 0, 1857928, 'I(1, 0, \'327884 tetrahedra\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 1', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 23872, 'I(1, 0, \'1383 matrix,    0KB disk\')', true, true)
		ProfileItem('  Solver DRS', 2, 0, 2, 0, 417297, 'I(1, 0, \'453067 matrix,    0KB disk\')', true, true)
		ProfileItem('  adapt', 43, 0, 41, 0, 2025472, 'I(1, 0, \'327884 tetrahedra\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 2', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('  Mesh (mixedmode, adaptive)', 41, 0, 41, 0, 579396, 'I(1, 0, \'426262 tets\')', true, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 36072, 'I(1, 0, \'3495 matrix,    0KB disk\')', true, true)
		ProfileItem('  Solver DRS', 8, 0, 8, 0, 1132553, 'I(1, 0, \'583679 matrix,    0KB disk\')', true, true)
		ProfileItem('  adapt', 57, 0, 56, 0, 2493440, 'I(1, 0, \'426262 tetrahedra\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 3', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('  Mesh (mixedmode, adaptive)', 59, 0, 59, 0, 671588, 'I(1, 0, \'554155 tets\')', true, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 44944, 'I(1, 0, \'8299 matrix,    0KB disk\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Process \\\'3dnms\\\' terminated abnormally.  It may have run out of memory or could have been killed by the user.\')', false, true)
		ProfileFootnote('I(1, 0, \'Time:  10/09/2022 02:21:12, Status: Error\')', 2)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2021
		MinorVer=1
		Name='Solution Process'
		StartInfo='Time:  10/10/2022 13:49:29; Host: cn3.it.auth.gr; Processor: 20; OS: Linux 3.10.0-1160.66.1.el7.x86_64; Maxwell 3D 2021.1.0'
		TotalInfo='Elapsed time : 00:07:13 , ComEngine Memory : 337 M'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /mnt/apps/prebuilt/AnsysEM/2021R1/AnsysEM21.1/Linux64/MAXWELLCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Desired RAM limit not set.\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:01:07 , Maxwell 3D ComEngine Memory : 324 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 2', 0, 0, 0, 0, 0, 'I(1, 0, \'Resolve field\')', false, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 36056, 'I(1, 0, \'3495 matrix,    0KB disk\')', true, true)
		ProfileItem('  Solver DRS', 11, 0, 11, 0, 1132553, 'I(1, 0, \'583679 matrix,    0KB disk\')', true, true)
		ProfileItem('  adapt', 85, 0, 84, 0, 2483200, 'I(1, 0, \'426262 tetrahedra\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 3', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('  Mesh (mixedmode, adaptive)', 78, 0, 78, 0, 675088, 'I(1, 0, \'554148 tets\')', true, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 45072, 'I(1, 0, \'6691 matrix,    0KB disk\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Process \\\'3dnms\\\' terminated abnormally.  It may have run out of memory or could have been killed by the user.\')', false, true)
		ProfileFootnote('I(1, 0, \'Time:  10/10/2022 13:56:43, Status: Error\')', 2)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2021
		MinorVer=1
		Name='Solution Process'
		StartInfo='Time:  10/10/2022 14:26:48; Host: cn3.it.auth.gr; Processor: 20; OS: Linux 3.10.0-1160.66.1.el7.x86_64; Maxwell 3D 2021.1.0'
		TotalInfo='Elapsed time : 00:07:00 , ComEngine Memory : 337 M'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /mnt/apps/prebuilt/AnsysEM/2021R1/AnsysEM21.1/Linux64/MAXWELLCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Desired RAM limit not set.\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:01:05 , Maxwell 3D ComEngine Memory : 324 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 2', 0, 0, 0, 0, 0, 'I(1, 0, \'Resolve field\')', false, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 36044, 'I(1, 0, \'3495 matrix,    0KB disk\')', true, true)
		ProfileItem('  Solver DRS', 12, 0, 12, 0, 1132553, 'I(1, 0, \'583679 matrix,    0KB disk\')', true, true)
		ProfileItem('  adapt', 85, 0, 85, 0, 2483200, 'I(1, 0, \'426262 tetrahedra\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 3', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('  Mesh (mixedmode, adaptive)', 80, 0, 80, 0, 671872, 'I(1, 0, \'554161 tets\')', true, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 44960, 'I(1, 0, \'8325 matrix,    0KB disk\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Process \\\'3dnms\\\' terminated abnormally.  It may have run out of memory or could have been killed by the user.\')', false, true)
		ProfileFootnote('I(1, 0, \'Time:  10/10/2022 14:33:48, Status: Error\')', 2)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2021
		MinorVer=1
		Name='Solution Process'
		StartInfo='Time:  10/11/2022 16:16:53; Host: cn12.it.auth.gr; Processor: 20; OS: Linux 3.10.0-1160.66.1.el7.x86_64; Maxwell 3D 2021.1.0'
		TotalInfo='Elapsed time : 00:00:00 , ComEngine Memory : 331 M'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /mnt/apps/prebuilt/AnsysEM/2021R1/AnsysEM21.1/Linux64/MAXWELLCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Desired RAM limit not set.\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , Maxwell 3D ComEngine Memory : 331 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Illegal Overlap:  some boundaries or excitations are assigned on the same entity. To continue, please resolve this condition.\')', false, true)
		ProfileFootnote('I(1, 0, \'Time:  10/11/2022 16:16:53, Status: Engine Detected Error\')', 2)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2021
		MinorVer=1
		Name='Solution Process'
		StartInfo='Time:  10/11/2022 17:04:38; Host: cn12.it.auth.gr; Processor: 20; OS: Linux 3.10.0-1160.66.1.el7.x86_64; Maxwell 3D 2021.1.0'
		TotalInfo='Elapsed time : 00:05:05 , ComEngine Memory : 347 M'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /mnt/apps/prebuilt/AnsysEM/2021R1/AnsysEM21.1/Linux64/MAXWELLCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Desired RAM limit not set.\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:54 , Maxwell 3D ComEngine Memory : 334 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 2', 0, 0, 0, 0, 0, 'I(1, 0, \'Resolve field\')', false, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 36068, 'I(1, 0, \'3495 matrix,    0KB disk\')', true, true)
		ProfileItem('  Solver DRS', 8, 0, 8, 0, 1132553, 'I(1, 0, \'583679 matrix,    0KB disk\')', true, true)
		ProfileItem('  adapt', 54, 0, 54, 0, 2493440, 'I(1, 0, \'426262 tetrahedra\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 3', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('  Mesh (mixedmode, adaptive)', 64, 0, 63, 0, 672860, 'I(1, 0, \'554150 tets\')', true, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 44996, 'I(1, 0, \'7906 matrix,    0KB disk\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Process \\\'3dnms\\\' terminated abnormally.  It may have run out of memory or could have been killed by the user.\')', false, true)
		ProfileFootnote('I(1, 0, \'Time:  10/11/2022 17:09:44, Status: Error\')', 2)
	$end 'ProfileGroup'
$end 'Profile'
