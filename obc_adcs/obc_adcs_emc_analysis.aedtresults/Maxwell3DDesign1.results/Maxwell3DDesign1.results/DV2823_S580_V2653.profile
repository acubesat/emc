$begin 'Profile'
	$begin 'ProfileGroup'
		MajorVer=2021
		MinorVer=1
		Name='Solution Process'
		StartInfo='Time:  10/09/2022 02:21:41; Host: cn12.it.auth.gr; Processor: 20; OS: Linux 3.10.0-1160.66.1.el7.x86_64; Maxwell 3D 2021.1.0'
		TotalInfo='Elapsed time : 00:03:44 , ComEngine Memory : 347 M'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /mnt/apps/prebuilt/AnsysEM/2021R1/AnsysEM21.1/Linux64/MAXWELLCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Desired RAM limit not set.\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:53 , Maxwell 3D ComEngine Memory : 334 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 1', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 23876, 'I(1, 0, \'1383 matrix,    0KB disk\')', true, true)
		ProfileItem('  Solver DRS', 2, 0, 2, 0, 417297, 'I(1, 0, \'453067 matrix,    0KB disk\')', true, true)
		ProfileItem('  adapt', 41, 0, 41, 0, 2025472, 'I(1, 0, \'327884 tetrahedra\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 2', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('  Mesh (mixedmode, adaptive)', 35, 0, 34, 0, 530460, 'I(1, 0, \'393507 tets\')', true, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 28256, 'I(1, 0, \'3479 matrix,    0KB disk\')', true, true)
		ProfileItem('  Solver DRS', 6, 0, 6, 0, 884985, 'I(1, 0, \'540501 matrix,    0KB disk\')', true, true)
		ProfileItem('  adapt', 47, 0, 47, 0, 2416640, 'I(1, 0, \'393507 tetrahedra\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Adaptive Passes converged\')', false, true)
		ProfileFootnote('I(1, 0, \'Time:  10/09/2022 02:25:25, Status: Normal Completion\')', 0)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2021
		MinorVer=1
		Name='Solution Process'
		StartInfo='Time:  10/10/2022 13:57:42; Host: cn3.it.auth.gr; Processor: 20; OS: Linux 3.10.0-1160.66.1.el7.x86_64; Maxwell 3D 2021.1.0'
		TotalInfo='Elapsed time : 00:06:38 , ComEngine Memory : 336 M'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /mnt/apps/prebuilt/AnsysEM/2021R1/AnsysEM21.1/Linux64/MAXWELLCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Desired RAM limit not set.\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:01:06 , Maxwell 3D ComEngine Memory : 324 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 2', 0, 0, 0, 0, 0, 'I(1, 0, \'Resolve field\')', false, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 28228, 'I(1, 0, \'3479 matrix,    0KB disk\')', true, true)
		ProfileItem('  Solver DRS', 9, 0, 8, 0, 884985, 'I(1, 0, \'540501 matrix,    0KB disk\')', true, true)
		ProfileItem('  adapt', 72, 0, 72, 0, 2406400, 'I(1, 0, \'393507 tetrahedra\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 3', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('  Mesh (mixedmode, adaptive)', 63, 0, 63, 0, 595936, 'I(1, 0, \'472223 tets\')', true, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 39556, 'I(1, 0, \'5572 matrix,    0KB disk\')', true, true)
		ProfileItem('  Solver DRS', 24, 0, 24, 0, 1743187, 'I(1, 0, \'647204 matrix,    0KB disk\')', true, true)
		ProfileItem('  adapt', 92, 0, 90, 0, 2789376, 'I(1, 0, \'472223 tetrahedra\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Adaptive Passes converged\')', false, true)
		ProfileFootnote('I(1, 0, \'Time:  10/10/2022 14:04:20, Status: Normal Completion\')', 0)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2021
		MinorVer=1
		Name='Solution Process'
		StartInfo='Time:  10/10/2022 14:34:26; Host: cn3.it.auth.gr; Processor: 20; OS: Linux 3.10.0-1160.66.1.el7.x86_64; Maxwell 3D 2021.1.0'
		TotalInfo='Elapsed time : 00:03:35 , ComEngine Memory : 337 M'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /mnt/apps/prebuilt/AnsysEM/2021R1/AnsysEM21.1/Linux64/MAXWELLCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Desired RAM limit not set.\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:01:06 , Maxwell 3D ComEngine Memory : 324 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 3', 0, 0, 0, 0, 0, 'I(1, 0, \'Resolve field\')', false, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 39556, 'I(1, 0, \'5572 matrix,    0KB disk\')', true, true)
		ProfileItem('  Solver DRS', 22, 0, 22, 0, 1743187, 'I(1, 0, \'647204 matrix,    0KB disk\')', true, true)
		ProfileItem('  adapt', 90, 0, 90, 0, 2789376, 'I(1, 0, \'472223 tetrahedra\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Adaptive Passes did not converge\')', false, true)
		ProfileFootnote('I(1, 0, \'Time:  10/10/2022 14:38:02, Status: Normal Completion\')', 0)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2021
		MinorVer=1
		Name='Solution Process'
		StartInfo='Time:  10/11/2022 16:17:09; Host: cn12.it.auth.gr; Processor: 20; OS: Linux 3.10.0-1160.66.1.el7.x86_64; Maxwell 3D 2021.1.0'
		TotalInfo='Elapsed time : 00:00:00 , ComEngine Memory : 334 M'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /mnt/apps/prebuilt/AnsysEM/2021R1/AnsysEM21.1/Linux64/MAXWELLCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Desired RAM limit not set.\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:00 , Maxwell 3D ComEngine Memory : 334 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Illegal Overlap:  some boundaries or excitations are assigned on the same entity. To continue, please resolve this condition.\')', false, true)
		ProfileFootnote('I(1, 0, \'Time:  10/11/2022 16:17:09, Status: Engine Detected Error\')', 2)
	$end 'ProfileGroup'
	$begin 'ProfileGroup'
		MajorVer=2021
		MinorVer=1
		Name='Solution Process'
		StartInfo='Time:  10/11/2022 17:10:09; Host: cn12.it.auth.gr; Processor: 20; OS: Linux 3.10.0-1160.66.1.el7.x86_64; Maxwell 3D 2021.1.0'
		TotalInfo='Elapsed time : 00:02:42 , ComEngine Memory : 345 M'
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Executing from /mnt/apps/prebuilt/AnsysEM/2021R1/AnsysEM21.1/Linux64/MAXWELLCOMENGINE.exe\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Desired RAM limit not set.\')', false, true)
		ProfileItem('Design Validation', 0, 0, 0, 0, 0, 'I(1, 0, \'Elapsed time : 00:00:54 , Maxwell 3D ComEngine Memory : 332 M\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Perform full validations\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'\')', false, true)
		ProfileItem('Pass 3', 0, 0, 0, 0, 0, 'I(1, 0, \'Resolve field\')', false, true)
		ProfileItem('  Solver DRS', 0, 0, 0, 0, 39584, 'I(1, 0, \'5572 matrix,    0KB disk\')', true, true)
		ProfileItem('  Solver DRS', 16, 0, 16, 0, 1743187, 'I(1, 0, \'647204 matrix,    0KB disk\')', true, true)
		ProfileItem('  adapt', 62, 0, 62, 0, 2799616, 'I(1, 0, \'472223 tetrahedra\')', true, true)
		ProfileItem('', 0, 0, 0, 0, 0, 'I(1, 0, \'Adaptive Passes did not converge\')', false, true)
		ProfileFootnote('I(1, 0, \'Time:  10/11/2022 17:12:51, Status: Normal Completion\')', 0)
	$end 'ProfileGroup'
$end 'Profile'
